import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.tealAccent[700],
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('images/lucy.jpg'),
              ),
              Text(
                "Lucy Kulkarni",
                style: TextStyle(
                  fontSize: 40.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Pacifico',
                ),
              ),
              Center(
                child: Text(
                  "GOD",
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.teal.shade100,
                    fontFamily: 'SourceSansPro',
                    letterSpacing: 2.5,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 20.0,
                child:Divider(
                  color: Colors.black,
                ),
                width:150,
              ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: ListTile(
                    leading: Icon(Icons.call),
                    title: Text(
                      "+91 9623023832",
                      style: TextStyle(
                          fontFamily: 'SourceSansPro',
                          fontSize: 20.0,
                          color: Colors.teal[900]
                      ),
                    ),
                  ),
                ),
                margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 25.0),

              ),

              Card(
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: ListTile(
                    leading: Icon(Icons.email),
                    title:Text(
                      "asatwe@gmail.com",
                      style: TextStyle(
                        fontFamily: 'SourceSansPro',
                        fontSize: 20.0,
                        color: Colors.teal[900]
                      ),
                    ),
                  ),
                ),
                margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 25.0),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
